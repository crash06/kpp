package Rieltor;
import Dweling.Dweling;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author tacjka
 */
public class Rieltor {
    ArrayList<Dweling> dwelings;
    float price;
    
    public Rieltor(ArrayList<Dweling> dwelings, float price) {
        this.dwelings = dwelings;
        this.price = price;  
    }
    
    public float getDwelingsCost() {
        float sum = 0;
        
        for (Dweling dweling : this.dwelings) {
            sum += dweling.getCost();
        }
        
        return sum + this.price;
    }
    
    public ArrayList<Dweling> getDwelings() {
        return this.dwelings;
    }
    
    public static class DistanceToSchoolDescComparator implements Comparator<Dweling> {       
        @Override
        public int compare(Dweling dweling1, Dweling dweling2) {
            return (int)(dweling1.getDistanceToSchool() - dweling2.getDistanceToSchool());     
        }
    }
    
    public static class DistanceToSchoolAscComparator implements Comparator<Dweling> {       
        @Override
        public int compare(Dweling dweling1, Dweling dweling2) {
            return (int)(dweling2.getDistanceToSchool() - dweling1.getDistanceToSchool());     
        }
    }
    
    public void sortbyDistanceToSchool (boolean desc) {
        if (desc) {
            Collections.sort(this.dwelings, new DistanceToSchoolDescComparator());     
        } else {
            Collections.sort(this.dwelings, new DistanceToSchoolAscComparator());
        } 
    }
    
    public static class CostDescComparator implements Comparator<Dweling> {       
        @Override
        public int compare(Dweling dweling1, Dweling dweling2) {
            return (int)(dweling1.getCost() - dweling2.getCost());
        }
    }
    
    public static class CostAscComparator implements Comparator<Dweling> {       
        @Override
        public int compare(Dweling dweling1, Dweling dweling2) {
            return (int)(dweling2.getCost() - dweling1.getCost());
        }
    }
    
    public void sortbyCost (boolean desc) {
        if (desc) {
            Collections.sort(this.dwelings, new CostDescComparator());
        } else {
            Collections.sort(this.dwelings, new CostAscComparator());
        }
    }
    
 
}
