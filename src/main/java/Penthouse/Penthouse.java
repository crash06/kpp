package Penthouse;
import Dweling.Dweling;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author tacjka
 */

public class Penthouse extends Dweling{
    int numberOfFloors;
    float poolArea;
    float terraceArea;
    
    public Penthouse(
        String street, 
        int buildingNumber,
        float costPerOneMetr,
        float area,
        int numberOfRooms,
        int distanceToSchool,
        int distanceToKindergarten,
        int numberOfFloors,
        float terraceArea,
        float poolArea
    ) {
        super(
            street, 
            buildingNumber, 
            costPerOneMetr, 
            area + terraceArea + poolArea, 
            numberOfRooms, 
            distanceToSchool, 
            distanceToKindergarten
        );
        this.numberOfFloors = numberOfFloors; 
        this.terraceArea = terraceArea; 
        this.poolArea = poolArea;
    }
}
