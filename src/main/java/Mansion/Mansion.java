package Mansion;
import Dweling.Dweling;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author tacjka
 */
public class Mansion extends Dweling {
    int numberOfFloors;
    int yardArea;
    
    public Mansion(
        String street, 
        int buildingNumber,
        float costPerOneMetr,
        float area,
        int numberOfRooms,
        int distanceToSchool,
        int distanceToKindergarten,
        int numberOfFloors,
        int yardArea
    ) {
        super(
            street, 
            buildingNumber, 
            costPerOneMetr, 
            area + yardArea, 
            numberOfRooms, 
            distanceToSchool, 
            distanceToKindergarten
        );
        this.numberOfFloors = numberOfFloors; 
        this.yardArea = yardArea;
    }
}
