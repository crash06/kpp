package Dweling;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author tacjka
 */
public class Dweling {
    String street;
    int buildingNumber;
    float costPerOneMetr;
    float area;
    int numberOfRooms;
    float distanceToSchool;
    float distanceToKindergarten;
    float cost;
    
    public Dweling(
        String street, 
        int buildingNumber,
        float costPerOneMetr,
        float area,
        int numberOfRooms,
        float distanceToSchool,
        float distanceToKindergarten
    ) {
        this.street = street;
        this.buildingNumber = buildingNumber;
        this.costPerOneMetr = costPerOneMetr;
        this.area = area;
        this.numberOfRooms = numberOfRooms;
        this.distanceToSchool = distanceToSchool;
        this.distanceToKindergarten = distanceToKindergarten;
        this.cost = costPerOneMetr * area;
    }
    
    public float getCost() {
        return this.cost;
    }
    
    public int getBuildingNumber() {
        return this.buildingNumber;
    }
    
    public float getArea() {
        return this.area;
    }
    
    public int getNumberOfRooms() {
        return this.numberOfRooms;
    }
    
    public String getStreet() {
        return this.street;
    }
    
    public float getCostPerOneMetr() {
        return this.costPerOneMetr;
    }
    
    public float getDistanceToSchool() {
        return this.distanceToSchool;
    }
    
    public float getDistanceToKindergarten() {
        return this.distanceToKindergarten;
    }
    
    @Override
    public String toString() 
    { 
        return(this.getStreet()+ this.getBuildingNumber() + 
               ".\nArea " +this.getArea()+
               "\nCost per metr" + this.getCostPerOneMetr() + 
               " rooms " + this.getNumberOfRooms() +
               " cost "+ this.getCost()+
               ".\nDistance to school - " +this.getDistanceToSchool() +
               ".\nDistance to kindergarten - " +this.getDistanceToKindergarten()); 
    } 
}
