package Apartament;
import Dweling.Dweling;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author tacjka
 */
public class Apartament extends Dweling {
    int floor;
    int apartamentNumber;
    
    public Apartament(
        String street, 
        int buildingNumber,
        float costPerOneMetr,
        float area,
        int numberOfRooms,
        int distanceToSchool,
        int distanceToKindergarten,
        int floor,
        int apartamentNumber
    ) {
        super(
            street, 
            buildingNumber, 
            costPerOneMetr, 
            area, 
            numberOfRooms, 
            distanceToSchool, 
            distanceToKindergarten
        );
        this.floor = floor; 
        this.apartamentNumber = apartamentNumber;
    }
}
