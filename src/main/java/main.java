import Dweling.Dweling; 
import Rieltor.Rieltor;
import Apartament.Apartament;
import Mansion.Mansion;
import Penthouse.Penthouse;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author tacjka
 */
public class main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Dweling dweling = new Dweling("Shuckhevicha", 86, 170, 90, 3, 2, 1);
        Apartament apartament = new Apartament("Chornovola", 42, 250, 55, 1, 0, 2, 4, 45);
        Mansion mansion = new Mansion("Bandery", 12, 550, 175, 5, 1, 0, 2, 35);
        Penthouse penthouse = new Penthouse("Shevchenka", 3, 450, 215, 7, 2, 3, 3, 15, 6);
        ArrayList<Dweling> dwelings = new ArrayList<>(Arrays.asList(dweling, apartament, mansion, penthouse));
        Rieltor riel = new Rieltor(dwelings, 200);
        Scanner input = new Scanner(System.in);
        riel.getDwelings().forEach((dwel) -> {
            System.out.println(dwel.toString());
        });
        System.out.println("Type command: ");
        String doSort = input.next();
        if(doSort.equals("sort")) {
            String sortBy = input.next();
            if("cost".equals(sortBy)) {
                String desc = input.next();
                if("desc".equals(desc)) {
                    riel.sortbyCost(true);
                }
                if("asc".equals(desc)) {
                    riel.sortbyCost(false);
                }
            }
            if("school".equals(sortBy)) {
                String desc = input.next();
                if("desc".equals(desc)) {
                    riel.sortbyDistanceToSchool(true);
                }
                if("asc".equals(desc)) {
                    riel.sortbyDistanceToSchool(false);
                }
            }
        }
        riel.getDwelings().forEach((dwel) -> {
            System.out.println(dwel.toString());
        });
    }
    
}
